package net.islandearth.anvillogin.api;

import net.islandearth.languagy.language.Translator;

public interface AnvilLoginAPI {
	/**
	 * Gets the translator provided by Languagy
	 * @return Translator
	 */
	Translator getTranslator();
}
